#Seção 1: importação das bibliotecas
#Aqui utilizamos 5 biliotecas específicas para a análise de paginas web, coleta e envio de dados para banco de dados NOSQL

#BeautifulSoup nos permite deixar o código html mais inteligível e trabalhar nele através de seus elementos
from bs4 import BeautifulSoup
#lxml ajuda na tradução dos dados para um formato mais organizado
from lxml import html
#requests nos ajuda a fazer requisições http sem a necessidade de abrir manualmente as páginas web
import requests
#pymongo nos permite a conexão com o banco de dados MongoDB
import pymongo
#datetime nos permite a manipulação de datas
import datetime
##################################################################################################################################

#Seção 2: Conexão ao banco de dados
#Aqui fazemos a conexão com o MongoDB através da String de conexão
conexao = pymongo.MongoClient("mongodb+srv://user123:user123@cluster-cpivz.mongodb.net/test?retryWrites=true")
#Aqui conectamos a nossa base de dados cotação
mydb = conexao['Cotacao']
##################################################################################################################################

#Seção 3: Manipulação de datas
#Aqui estabelecemos algumas variáveis para a manipulação das datas presentes na coleta de informações
date = datetime.date(2017,1,1)##Data Histórica para iniciar a coleta de dados (2013-08-13)
today = datetime.date.today()##Dia Atual
oneday = datetime.timedelta(days = 1)##Contagem de dias

#Criação de calendário para percorrer as páginas
date_counter = 0
def genDates(someDate):#Definição da funcão do calendário, modificando os dias
        while someDate != today:
                someDate += oneday
                yield someDate
for d in genDates(date):#inicio da Repetição do 1 dia, inicia no primeiro dia escolhido na data histórica até a data de hoje
        date_counter += 1
        dia = int (d.day)
        mes = int (d.month)
        ano = int (d.year)
        if mes <10: #Concatenando meses com '0'
                string_mes = str(mes)
                mes_concatenado = '0'+string_mes
        else:
                mes_concatenado=str(mes)
        if dia <10: #Concatenando dias com '0'
                string_dia= str(dia)
                dia_concatenado = '0'+string_dia
        else:
                dia_concatenado=str(dia)
        #Seção 4: Coleta de dados
        #Montagem do link para a conexão com a página
        link_ano = str('https://www.noticiasagricolas.com.br/cotacoes/soja/soja-mercado-fisico-sindicatos-e-cooperativas/%d' %ano )
        link_mes = str(link_ano +'-%s' %mes_concatenado)
        link_dia = str(link_mes +'-%s'%dia_concatenado)
        link = str(link_dia)
        #Conexão com a página através do requests
        url = requests.get(link)
        #Aquisição dos dados em formato html através do Beautifulsoup
        soup = BeautifulSoup(url.content, 'html.parser')
        #Seleção das informações pertinentes dentro do código html
        preco = soup.select('table.cot-fisicas tbody tr')
        data = soup.select('div.info')
        #Seção 5: manipulação dos dados obtidos
        #Percorrendo os dados obtidos
        for x in data:
                for i in preco:
                        item = i.text
                        #Fatiamento das informações em elementos separados
                        informacoes = str(item).strip().split('\n')
                        #manipulação das datas para tranformá-las em tipo datetime
                        databr = str("{}/{}/{}".format(dia_concatenado,mes_concatenado,ano))
                        dataAtual = str(databr).split('/')[::-1]
                        dataAtual = str(dataAtual[0] + '-' + dataAtual[1] + '-' + dataAtual[2])
                        dataAtual = datetime.datetime.strptime(dataAtual, "%Y-%m-%d")
                        #Tratamento de excessões que possam poluir a visualização causando erros
                        if len(informacoes) < 3:
                                informacoes.extend(['0,0'])#acrecimo de uma informação faltante
                        if (informacoes[1].startswith("S") or informacoes[1].startswith("s") or informacoes[1].endswith("-") or informacoes[1].startswith("F")):
                                informacoes[1]="0,0"

                        if (informacoes[2].endswith("-") or informacoes[2].startswith("F") or informacoes[2].startswith("")):
                                informacoes[2]="0,0"
                        confirma_valor = float(informacoes[1].split('(')[0].replace(',', '.'))
                        if (confirma_valor > 0):
                                #impressão das informações na tela
                                print("Data: {} || Cidade: {} || Preço: {} || Variação: {}".format(dataAtual,informacoes[0],informacoes[1],informacoes[2]))
                                #Seção 6: inserção de dados no MongoDB
                                mydb.TesteSoja.update(
                                        {
                                                "data" : dataAtual,
                                                "cidade" : informacoes[0].split('(')[0],
                                                "valor" : float(informacoes[1].split('(')[0].replace(',', '.')),
                                                "variante" : float(informacoes[2].replace(',', '.'))
                                        }, #verificação de informações duplicadas na inserção
                                        {
                                                "data" : dataAtual,
                                                "cidade" : informacoes[0].split('(')[0],
                                                "valor" : float(informacoes[1].split('(')[0].replace(',', '.')),
                                                "variante" : float(informacoes[2].replace(',', '.'))
                                        },
                                        upsert = True
                                )
                        