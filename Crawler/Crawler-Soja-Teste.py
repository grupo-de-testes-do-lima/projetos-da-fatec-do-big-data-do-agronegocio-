from bs4 import BeautifulSoup
from lxml import html
import requests
import pymongo
import datetime

request = requests.get('https://www.noticiasagricolas.com.br/cotacoes/soja/soja-mercado-fisico-sindicatos-e-cooperativas/2019-04-24')

soup=BeautifulSoup(request.content,'html.parser')

tabela = soup.select('table.cot-fisicas tbody tr')
data = soup.select('div.info')

for i in data:
    for x in tabela:
        item = x.text
        y = str(item).strip().split('\n')
        j = i.text.split()

        print(j[1],y[0],y[1],y[2])