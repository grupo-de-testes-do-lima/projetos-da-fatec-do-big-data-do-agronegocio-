from bs4 import BeautifulSoup
from lxml import html
import requests
import pymongo
import datetime

conexao = pymongo.MongoClient("mongodb+srv://user:user@cluster-cpivz.mongodb.net/test?retryWrites=true")
mydb = conexao['Cotacao']

request= requests.get('https://www.noticiasagricolas.com.br/cotacoes/soja/soja-mercado-fisico-sindicatos-e-cooperativas')

soup=BeautifulSoup(request.content,'html.parser')

tabela = soup.select('table.cot-fisicas tbody tr')
data = soup.select('div.cotacao div.info div.fechamento')

for i in data:
    for x in tabela:
        data = i.text
        data = data.split()
        
        item = x.text
        item2 = str(item).strip().split('\n')

        dataAtual = str(data[1]).split('/')[::-1]
        dataAtual = str(dataAtual[0] + '-' + dataAtual[1] + '-' + dataAtual[2])
        dataAtual = datetime.datetime.strptime(dataAtual, "%Y-%m-%d")

        print ('DATA : {} || DADOS : {}'.format(data[1],item2))

        if (item2[1] == "s/ cotação"):
                item2[1]="0,0"
        if (item2[1] == "s/cotação"):
                item2[1]="0,0"
        
        if (item2[2] == "-"):
                item2[2]="0,0"
        mydb.Soja.update(
                            {
                                "data" : dataAtual,
                                "praça" : item2[0],
                                "Preço" : float(item2[1].replace(',', '.')),
                                "variação" : float(item2[2].replace(',', '.'))
                            },
                            {
                                "data" : dataAtual,
                                "praça" : item2[0],
                                "Preço" : float(item2[1].replace(',', '.')),
                                "variação" : float(item2[2].replace(',', '.'))
                            },
                            upsert = True
                        )
