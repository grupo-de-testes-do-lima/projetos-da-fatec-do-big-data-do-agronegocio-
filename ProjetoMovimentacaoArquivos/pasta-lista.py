import glob
import os
import shutil

arquivos = glob.glob("/home/gabriel/Imagens/*.jpeg")

for i in arquivos:
    x = i.split("/")
    y = x[4].split("-")
    caminho = x[4]
    nome = str(y[0])
    pasta =str( '/home/gabriel/pastaGerada/%s' %nome)

    try:  
        os.mkdir(pasta)
        shutil.move('/home/gabriel/Imagens/%s' %caminho, '/home/gabriel/pastaGerada/%s/%s' %(nome,caminho))
    except OSError:  
        print ("Envio para a pasta %s Já criada" % pasta)
        shutil.move('/home/gabriel/Imagens/%s' %caminho, '/home/gabriel/pastaGerada/%s/%s' %(nome,caminho))
    else:  
        print ("Successfully created the directory %s " % pasta)
