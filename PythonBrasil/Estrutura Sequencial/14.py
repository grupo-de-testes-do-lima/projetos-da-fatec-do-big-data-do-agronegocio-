peso_peixes = float(input("Insia o peso dos peixes em KG  "))
if peso_peixes > 50:
    peso_excesso = peso_peixes - 50
    multa = peso_excesso * 4
else:
    peso_excesso = 0
    multa = 0
print(f"O peso informado foi {peso_peixes} o excesso foi de {peso_excesso} e a multa de R${multa}")