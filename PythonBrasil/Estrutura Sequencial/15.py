reais_hora = float(input("Quanto você ganha por hora? \n"))
horas_trabalhadas = float(input("Quantas horas trabalha por mês? \n"))

salario_bruto = horas_trabalhadas*reais_hora
ir = salario_bruto*0.11
inss = salario_bruto*0.08
sindicato = salario_bruto*0.05
salario_liquido  = salario_bruto - ir - inss - sindicato


print(f"Salário Bruto : R${salario_bruto}\nIR (11%) : R${ir}\nINSS (8%) : R${inss}\nSindicato (5%) : R${sindicato}\nSalario liquido :R${salario_liquido}")