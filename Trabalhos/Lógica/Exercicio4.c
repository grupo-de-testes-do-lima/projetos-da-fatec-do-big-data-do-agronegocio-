 #include <stdio.h>
 int mmc(int num1, int num2) {

        int resto, a, b;

        a = num1;
        b = num2;

        do {
            resto = a % b;

            a = b;
            b = resto;

        } while (resto != 0);

        return ( num1 * num2) / a;
    }

    int main(void)
    {
      int n1,n2, resposta;
      printf("Digite dois Numeros para calcular o mmc\n");
      scanf("%d",&n1);
      scanf("%d",&n2);

      resposta = mmc(n1,n2);
      printf("O MMC de %d e %d é igual a %d",n1,n2,resposta);
      /* gcc nome.c -o nome
        ./nome.c */
    }