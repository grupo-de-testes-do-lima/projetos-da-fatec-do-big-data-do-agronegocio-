;import requests
from bs4 import BeautifulSoup
import csv

listaItens = ['camisetas','polos','camisetas-regatas']

arquivo = open('C:\\Users\\adria\\Desktop\\dados.csv','w')

for categoria in listaItens:
    contador = 1

    while contador < 11:
        requisicao = requests.get('https://www.dafiti.com.br/roupas-masculinas/'+str(categoria)+'/?page='+str(contador))

        soup = BeautifulSoup(requisicao.content,'html.parser')

        marca = soup.find_all('div',{'class':'product-box-brand'})

        descricao =  soup.find_all('p',{'class':'product-box-title hide-mobile'})

        preco = soup.find_all('span',{'class':'product-box-price-from'})

        for nome,desc,price in zip(marca,descricao,preco):
            inserir =  str(nome.text)+';'+str(desc.text)+';'+str(price.text)+'\n'
            arquivo.write(inserir)
            print (inserir)

        contador+=1
arquivo.close()