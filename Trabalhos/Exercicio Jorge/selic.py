import requests
from lxml import html

#Até aqui, Nós importamos as bibliotecas

requisicao = requests.get('https://br.advfn.com/indicadores/taxa-selic/valores-historicos') #Aqui nós colocamos o site para a requisição

tree = html.fromstring(requisicao.content) #Aqui convertemos o html para o python conseguir trabalhar com ele

contador = 2 #Esse é o contador que vai variar as colunas da tabela
 #Aqui é o valor inicial das linhas

while contador <=229: #Aqui nós falamos que enquanto a linha for menor que 11, ou seja, até 10, ele vai rodar o laço de repetição 
    data = tree.xpath('//*[@id="section_1"]/table/tbody/tr[%d]/td[1]/text()' %contador) #Aqui entra o xpath, não se esqueça de inserir o /text() ao final dele!
    
    data1 = ''.join(data)

    entredata = tree.xpath('//*[@id="section_1"]/table/tbody/tr[%d]/td[2]/text()' %contador) #Aqui entra o xpath, não se esqueça de inserir o /text() ao final dele!
   
    entredata1 = ''.join(entredata)

    porcentagem = tree.xpath('//*[@id="section_1"]/table/tbody/tr[%d]/td[3]/text()' %contador) #Aqui entra o xpath, não se esqueça de inserir o /text() ao final dele!
    
    porcentagem1 = ''.join(porcentagem)

    print (str(data1)+' | '+str(entredata1)+' | '+str(porcentagem1)) #aqui vamos printar as 3 variaveis

    contador = contador + 1 #aqui estamos somando 1 toda vez que o laço se repete

print ('FIM DO BOT') #Aqui confirmamos a saida do Bot