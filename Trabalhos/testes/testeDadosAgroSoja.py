import requests
from lxml import html

#Até aqui, Nós importamos as bibliotecas

requisicao = requests.get('https://censoagro2017.ibge.gov.br/templates/censo_agro/resultadosagro/agricultura.html?localidade=0&tema=76518') #Aqui nós colocamos o site para a requisição

tree = html.fromstring(requisicao.content) #Aqui convertemos o html para o python conseguir trabalhar com ele

contador = 1 #Esse é o contador que vai variar as colunas da tabela
 #Aqui é o valor inicial das linhas

while contador <=30: #Aqui nós falamos que enquanto a linha for menor que 11, ou seja, até 10, ele vai rodar o laço de repetição 
    data = tree.xpath('//*[@id="mapa-coropletico--tabela"]/table/tbody/tr[%d]/td[1]/text()' %contador) #Aqui entra o xpath, não se esqueça de inserir o /text() ao final dele!
    

    entredata = tree.xpath('//*[@id="mapa-coropletico--tabela"]/table/tbody/tr[%d]/td[2]/text()' %contador) #Aqui entra o xpath, não se esqueça de inserir o /text() ao final dele!
   

    porcentagem = tree.xpath('//*[@id="mapa-coropletico--tabela"]/table/tbody/tr[%d]/td[3]/text()' %contador) #Aqui entra o xpath, não se esqueça de inserir o /text() ao final dele!
    

    print (str(data)+' | '+str(entredata)+' | '+str(porcentagem)) #aqui vamos printar as 3 variaveis

    contador = contador + 1 #aqui estamos somando 1 toda vez que o laço se repete

print ('FIM DO BOT') #Aqui confirmamos a saida do Bot