import requests
from lxml import html

#Até aqui, Nós importamos as bibliotecas

requisicao = requests.get('https://www.climatempo.com.br/brasil') #Aqui nós colocamos o site para a requisição

tree = html.fromstring(requisicao.content) #Aqui convertemos o html para o python conseguir trabalhar com ele

contador = 1 #Esse é o contador que vai variar as colunas da tabela
linha = 1 #Aqui é o valor inicial das linhas

while linha <11: #Aqui nós falamos que enquanto a linha for menor que 11, ou seja, até 10, ele vai rodar o laço de repetição 
    cidade = tree.xpath('///*[@id="mega-destaque"]/div[4]/div[1]/div[3]/div[3]/table[1]/tbody/tr[%d]/td[1]/text()' %contador) #Aqui entra o xpath, não se esqueça de inserir o /text() ao final dele!
    minimo = tree.xpath('//*[@id="mega-destaque"]/div[4]/div[1]/div[3]/div[3]/table[1]/tbody/tr[%d]/td[2]/span[2]/text()' %contador) #Aqui entra o xpath, não se esqueça de inserir o /text() ao final dele!
    maximo = tree.xpath('//*[@id="mega-destaque"]/div[4]/div[1]/div[3]/div[3]/table[1]/tbody/tr[%d]/td[3]/span[2]/text()' %contador) #Aqui entra o xpath, não se esqueça de inserir o /text() ao final dele!
    
    print (str(cidade)+' | '+str(minimo)+' | '+str(maximo)) #aqui vamos printar as 3 variaveis

    linha = linha + 1 #aqui estamos somando 1 toda vez que o laço se repete
    contador = contador + 1 #aqui estamos somando 1 toda vez que o laço se repete

print ('FIM DO BOT') #Aqui confirmamos a saida do Bot

'''
ALGUMAS DICAS PARA AJUDAR

Na linha 14,15 e 16 temos que inserir o xpath, inserir o /text() ao final dele e descobrir o que varia para colocarmos nosso contador no lugar!
O ideal é que ele fique como esse exemplo:
tree.xpath('/html/body/div[2]/main/div[%d]/div/div/text()' %contador) (ISTO É APENAS UM EXEMPLO!!!)

Boa sorte a todos! Estou disponivel no whats para duvidas!
'''