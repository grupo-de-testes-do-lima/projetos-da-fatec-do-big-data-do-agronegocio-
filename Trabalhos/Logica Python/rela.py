import tkinter as tk
from platform import python_version

def main(args):
    root = tk.Tk()
    root.geometry("220x80+30+30")

    l1= tk.Label(root, text="Texto superior", bg="red",fg="white").grid(row=0,column=0)
    l2= tk.Label(root, text="Texto intermediario", bg="yellow",fg="black").grid(row=1,column=1)
    l3= tk.Label(root, text="Texto inferior", bg="green",fg="white").grid(row=2,column=0)


    root.mainloop()
    return 0

if __name__=='__main__':
    print (python_version())
    import sys    
    sys.exit(main(sys.argv))