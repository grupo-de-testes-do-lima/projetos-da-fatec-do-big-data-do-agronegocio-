import requests, json, lxml.html

cookies = {
    'CGIC': 'CgtmaXJlZm94LWItZCJKdGV4dC9odG1sLGFwcGxpY2F0aW9uL3hodG1sK3htbCxhcHBsaWNhdGlvbi94bWw7cT0wLjksaW1hZ2Uvd2VicCwqLyo7cT0wLjg',
    '1P_JAR': '2019-03-08-19',
    'NID': '178=dBl0uIsFoyaJ4i_p9g94qVpz7sf-PS7fmb18Ies9OGDAzGD12KeE3FCaT8_SMZoYzr_WGdU9NC0gNNjsrVQ3lvVKgxxCVrR7XCMR6bdKCkycmp3qHTdw0XAAB2hta9iEyTNn7YxPnv-Qzrlm9NL5hhKe5qw6fC6HapgNlNEFH74',
    'ANID': 'AHWqTUlXq09CcxHikdMKFbPSsscEBlaroBnr6eH0gcUwRH6awvX8SK1NbPbQ_RoX',
}

headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:65.0) Gecko/20100101 Firefox/65.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,/;q=0.8',
    'Accept-Language': 'pt-BR,pt;q=0.8,en-US;q=0.5,en;q=0.3',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'TE': 'Trailers',
}

params = (
    ('client', 'firefox-b-d'),
    ('q', 'Agro'),
)
#Site
response = requests.get('https://www.noticiasagricolas.com.br/cotacoes/boi', headers=headers, params=params, cookies=cookies)
doc = lxml.html.fromstring(response.content)


i = 0
while(i <= 2):
    i += 1
    td = doc.xpath('/html/body/div[3]/div[4]/section/div[4]/div[2]/div[1]/div[2]/table/tbody/tr[%i]/td[1]' %i)[0]
    preco = doc.xpath('/html/body/div[3]/div[4]/section/div[4]/div[2]/div[1]/div[2]/table/tbody/tr[%i]/td[2]' %i)[0]
    peso = doc.xpath('/html/body/div[3]/div[4]/section/div[4]/div[2]/div[1]/div[2]/table/tbody/tr[%i]/td[3]' %i)[0]
    print(td.text)
    print(preco.text)
    print(peso.text)